using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
  public static UIManager instance;

  public GameObject startMenu;
  public InputField usernameField;

  private void Awake()
  {
    if (instance == null)
    {
      instance = this;
    }
    else
    {
      Debug.Log("Intance already exist, destroying object!");
      Destroy(this);
    }
  }

  public void ConnectToServer()
  {
    startMenu.SetActive(false);
    usernameField.interactable = false;
    Client.instance.ConnectToServer();
  }

}