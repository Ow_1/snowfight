﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OW_PlayerShoot : MonoBehaviour
{
    public float throwForce = 40f;
    public Transform SnowballPrefab;
    public Transform SnowBallSpawnPostion;

    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Transform snowball = Instantiate(SnowballPrefab, SnowBallSpawnPostion.position, SnowBallSpawnPostion.rotation);
            Rigidbody rb = snowball.GetComponent<Rigidbody>();
            rb.AddForce(SnowBallSpawnPostion.forward * throwForce, ForceMode.VelocityChange);
        }
    }
}
