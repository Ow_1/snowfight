﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
  public static Dictionary<int, Projectile> projectiles = new Dictionary<int, Projectile>();
  private static int nextProjectileId = 1;

  public int id;
  public Rigidbody rg;
  public int thrownByPlayer;
  public Vector3 initialForce;

  private void Start()
  {
    id = nextProjectileId;
    nextProjectileId++;
    projectiles.Add(id, this);

    ServerSend.SpawnProjectile(this, thrownByPlayer);

    rg.AddForce(initialForce);
  }

  private void FixedUpdate()
  {
    ServerSend.ProjectilePosition(this);
  }

  private void OnCollisionEnter(Collision other)
  {
    if (other.collider.CompareTag("Player"))
    {
      other.collider.GetComponent<Player>().TakeDamage(25);
    }
  }

  public void Initialize(Vector3 _initialMovemntDirection, float _initialForceStength, int _thrownByPlayer)
  {
    initialForce = _initialMovemntDirection * _initialForceStength;
    thrownByPlayer = _thrownByPlayer;
  }
}